//
//  UserDataService.swift
//  Smack
//
//  Created by mahmoud farid on 3/28/18.
//  Copyright © 2018 mahmoud farid. All rights reserved.
//

import Foundation

class UserDataService {
    static let instance = UserDataService()
    
    public private(set) var id = ""
    public private(set) var avatarColor = ""
    public private(set) var avatarName = ""
    public private(set) var email = ""
    public private(set) var name = ""

 
    func setUserData(id: String, avatarColor: String, avatarName: String, email: String, name:String){
        self.id = id
        self.avatarColor = avatarColor
        self.avatarName = avatarName
        self.email = email
        self.name = name
    }
    func setAvatarName(avatarName: String){
        self.avatarName = avatarName
    }
    func logOut(){
        self.id = ""
        self.avatarName = ""
        self.email = ""
        self.name = ""
        self.avatarColor = ""
        AuthService.instance.isLogedIn = false
        AuthService.instance.userEmail = ""
        AuthService.instance.authToken = ""
    }

    
    
    
}

