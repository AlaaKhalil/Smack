//
//  AvatarCell.swift
//  Smack
//
//  Created by mahmoud farid on 4/4/18.
//  Copyright © 2018 mahmoud farid. All rights reserved.
//

import UIKit

enum AvatarType{
    case dark
    case light
}

class AvatarCell: UICollectionViewCell {
    
    @IBOutlet weak var avatarImag: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        setUpView()
    }
    
    func configCell(index: Int, type: AvatarType){
        if type == .dark {
            avatarImag.image = UIImage(named:"dark\(index)")
            self.layer.backgroundColor = UIColor.lightGray.cgColor
        }
        else{
            avatarImag.image = UIImage(named:"light\(index)")
            self.layer.backgroundColor = UIColor.gray.cgColor

        }
        
    }
    func setUpView(){
        self.layer.backgroundColor = UIColor.lightGray.cgColor
        self.layer.cornerRadius = 10
        self.clipsToBounds = true
    }
}
