//
//  Constants.swift
//  Smack
//
//  Created by mahmoud farid on 3/22/18.
//  Copyright © 2018 mahmoud farid. All rights reserved.
//

import Foundation

//url constants
let BASE_URL = "https://firstchaty.herokuapp.com/v1/"
let URL_REGISTER = "\(BASE_URL)account/register"
let URL_LOGIN = "\(BASE_URL)account/login"
let ADD_USER = "\(BASE_URL)user/add"

// passing bool, return nothing
typealias CompletionHandler = (_ Success: Bool)->()

//user defaults
let TOKEN_KEY = "token"
let LOGGED_IN_KEY = "loggedIn"
let USER_EMAIL = "userEmail"


//headers & body
let HEADER = [ "Content-Type": "application/json; charset=utf-8"]

// Notification Constants
let NOTIF_USER_DATA_CHANGED = Notification.Name("notifUserDataChanged")

