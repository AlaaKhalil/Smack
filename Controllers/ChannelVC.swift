//
//  ChannelVC.swift
//  Smack
//
//  Created by mahmoud farid on 3/19/18.
//  Copyright © 2018 mahmoud farid. All rights reserved.
//

import UIKit

class ChannelVC: UIViewController {
    @IBOutlet weak var logedBtn: UIButton!
    @IBOutlet weak var userImage: UIImageView!
    @IBAction func prepareForUnwind(segue: UIStoryboardSegue){}
    override func viewDidLoad() {
        super.viewDidLoad()
        self.revealViewController().rearViewRevealWidth = self.view.frame.size.width - 60
        // listen for specific notificatiion
        NotificationCenter.default.addObserver(self, selector: #selector(ChannelVC.userDataChanged(_:)), name: NOTIF_USER_DATA_CHANGED , object: nil)
    
}
    @IBAction func loginBtn(_ sender: Any) {
        if AuthService.instance.isLogedIn{
            // show profile page
            let profile = ProfileVC()
            profile.modalPresentationStyle = .custom
            present(profile, animated: true, completion: nil)

        }
        else{
            performSegue(withIdentifier: "ToLoginVC", sender: nil)
        }
    }
    
    
    // this func is called every time we receive notifacation
    @objc func userDataChanged(_ notif: Notification){
        // if any user loged
        if AuthService.instance.isLogedIn {
            logedBtn.setTitle(UserDataService.instance.name.uppercased(), for: .normal)
            userImage.image = UIImage(named:UserDataService.instance.avatarName)
        }
        else {
            logedBtn.setTitle("login", for: .normal)
            userImage.image = UIImage(named: "menuProfileIcon")
            userImage.backgroundColor = UIColor.clear
            
        }
    }
}
