//
//  CreateAccountVC.swift
//  Smack
//
//  Created by mahmoud farid on 3/21/18.
//  Copyright © 2018 mahmoud farid. All rights reserved.
//

import UIKit

class CreateAccountVC: UIViewController {

    @IBOutlet weak var userImage: UIImageView!
    @IBOutlet weak var passwordTxtField: UITextField!
    @IBOutlet weak var emailTxtField: UITextField!
    @IBOutlet weak var usernameTxtfield: UITextField!
    
    //variables
    var avatarName = "profileDefault"
    var avatarColor = "[0.5, 0.5, 0.5, 1]"
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    override func viewDidAppear(_ animated: Bool) {
        if UserDataService.instance.avatarName != ""{
            userImage.image = UIImage(named: UserDataService.instance.avatarName)
            avatarName = UserDataService.instance.avatarName
        }
    }
    @IBAction func createAccountBtn(_ sender: Any) {
        guard let name = usernameTxtfield.text , usernameTxtfield.text != " " else {return}
        guard let email = emailTxtField.text , emailTxtField.text != "" else {return}
        guard let pass = passwordTxtField.text , passwordTxtField.text != "" else {return}
        AuthService.instance.registerUser(email: email, password: pass) { (success) in
            if success{
                AuthService.instance.logIn(email: email, password: pass, completion: { (success) in
                    if(success){
                        AuthService.instance.createUser(name: name, avatarColor: self.avatarColor, avatarName: self.avatarName, email: email, complition: { (success) in
                            if success{
                                print(UserDataService.instance.name)
                                self.performSegue(withIdentifier: "ToChannelVC", sender: nil)
                                
                                NotificationCenter.default.post(name: NOTIF_USER_DATA_CHANGED, object: nil)
                            }
                        })
                        
                    }
                })
            }
        }
    }
    @IBAction func pickAvatrBtn(_ sender: Any) {
    }
    
    @IBAction func pickBGBtn(_ sender: Any) {
    }
    @IBAction func closedBtn(_ sender: Any) {
        performSegue(withIdentifier: "ToChannelVC", sender: nil)
    }
}
