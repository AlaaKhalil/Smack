//
//  ProfileVC.swift
//  Smack
//
//  Created by mahmoud farid on 4/25/18.
//  Copyright © 2018 mahmoud farid. All rights reserved.
//

import UIKit

class ProfileVC: UIViewController {
  
    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var profileName: UILabel!
    @IBOutlet weak var profileEmail: UILabel!
    @IBOutlet weak var bgView: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpView()
        
    }

    @IBAction func closeBtn(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func logoutBtn(_ sender: Any) {
        UserDataService.instance.logOut()
        NotificationCenter.default.post(name: NOTIF_USER_DATA_CHANGED, object: nil)
        dismiss(animated: true, completion: nil)
    }
    
    func setUpView(){
        profileImage.image = UIImage(named: UserDataService.instance.avatarName)
        profileName.text = UserDataService.instance.name
        profileEmail.text = UserDataService.instance.email
        let closeTouch = UITapGestureRecognizer(target: self, action: #selector(ProfileVC.closeTap(_:)))
        bgView.addGestureRecognizer(closeTouch)
    }
    @objc func closeTap(_ recognizer: UITapGestureRecognizer){
        dismiss(animated: true, completion: nil)
    }
    
}
